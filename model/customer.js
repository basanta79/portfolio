'use strict'

const Sequelize = require('sequelize')
const db = require('../lib/connectionDB')

const client = db.define('Cliente', {
    CodCliente: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true, allowNull: false },
    RazonSocial: { type: Sequelize.STRING(50) },
    Nombre: { type: Sequelize.STRING(50) },
    CIFDNI: { type: Sequelize.STRING(20) },
    Domicilio: { type: Sequelize.STRING(100) },
    CodigoPostal: { type: Sequelize.STRING(10) },
    Poblacion: { type: Sequelize.STRING(50) },
    CodProvincia: { type: Sequelize.STRING(4) },
    CodPais: { type: Sequelize.STRING(3) },
    CodTarifa: { type: Sequelize.STRING(10) },
    TipoCliente: { type: Sequelize.BOOLEAN },
    RiesgoMaximo: { type: Sequelize.FLOAT },
    RiesgoAviso: { type: Sequelize.FLOAT },
    RiesgoAseguradora: { type: Sequelize.STRING(50) },
    CodFrecFact: { type: Sequelize.STRING(5) },
    Cesado: { type: Sequelize.BOOLEAN },
    FechaCese: { type: Sequelize.DATE },
    Bloqueado: { type: Sequelize.BOOLEAN },
    FechaBloqueo: { type: Sequelize.DATE },
    MotivoBloqueo: { type: Sequelize.STRING(50) },
    SaldoActual: { type: Sequelize.FLOAT },
    DtoTienda: { type: Sequelize.FLOAT },
    Provincia: { type: Sequelize.STRING(50) },
    MODPAGO: { type: Sequelize.STRING(30) },
    MEDPAGO: { type: Sequelize.STRING(30) },
    DIAPAGO: { type: Sequelize.INTEGER },
    FechaRiesgo: { type: Sequelize.DATE },
}, {
    timestamps: false,
})

module.exports = client;
