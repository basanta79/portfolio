'use strict'

const Sequelize = require('sequelize')
const db = require('../lib/connectionDB')

const card = db.define('Tarjetas', {
    CodTarjeta: { type: Sequelize.STRING(20), primaryKey: true, allowNull: false },
    Emisor: { type: Sequelize.STRING(4) },
    Estacion: { type: Sequelize.STRING(2) },
    Cliente: { type: Sequelize.STRING(6) },
    Vehiculo: { type: Sequelize.STRING(3) },
    DC: { type: Sequelize.STRING(1) },
    CodCliente: { type: Sequelize.INTEGER },
    TipoTarjeta: { type: Sequelize.INTEGER },
    PIN: { type: Sequelize.STRING(4) },
    CodVehiculo: { type: Sequelize.STRING(20) },
    FechaAlta: { type: Sequelize.DATE },
    Cesado: { type: Sequelize.BOOLEAN, allowNull: false },
    FechaCese: { type: Sequelize.DATE },
    Bloqueado: { type: Sequelize.BOOLEAN, allowNull: false },
    FechaBloqueo: { type: Sequelize.DATE },
    MotivoBloqueo: { type: Sequelize.STRING(50) },
    LimiteDiario: { type: Sequelize.FLOAT },
    SaldoActual: { type: Sequelize.FLOAT },
    NombreAsociado: { type: Sequelize.STRING(35) },
    AccionDescuento: { type: Sequelize.STRING(20) },
    Colectivo: { type: Sequelize.STRING(20) },
    LimiteSemanal: { type: Sequelize.FLOAT },
    NumTxDiario: { type: Sequelize.INTEGER },
    NumTxSemanal: { type: Sequelize.INTEGER },
}, {
    timestamps: false,
});

module.exports = card