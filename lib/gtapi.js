'use strict'

require('dotenv').config()
const axios = require('axios')

/* axios({
    method: 'get',
    url: process.env.GLOBALTANK_URL_CUSTOMERS,
    headers: {
        Authorization: process.env.GLOBALTANK_TOKEN
    }
}).then( httpResponse => {
    console.log(httpResponse.data)
}).catch( data => {
    console.log(data.response.data)
}) */

function Gtapi (name) {
    this.nombre=name

}

function getFromApi(url){
    return axios({
        method: 'get',
        url: url,
        headers: {
            Authorization: process.env.GLOBALTANK_TOKEN
        }
    })
}

Gtapi.prototype.readAllCustomers = function() {
    return getFromApi(process.env.GLOBALTANK_URL_CUSTOMERS)
}

Gtapi.prototype.readNewCustomers = function() {
    return getFromApi(process.env.GLOBALTANK_URL_NEW_CUSTOMERS)
}

module.exports = {
    Gtapi: Gtapi
}