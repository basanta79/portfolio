'use strict';

require('dotenv').config()

const Sequelize = require('sequelize');

// Option 1: Passing parameters separately
const sequelize = new Sequelize(process.env.MSSQL_DB, process.env.MSSQL_USER, process.env.MSSQL_PASS, {
  host: process.env.MSSQL_HOST,
  dialect: 'mssql',
  port: process.env.MSSQL_PORT,
});

module.exports = sequelize;